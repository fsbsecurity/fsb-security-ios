#import "KLCertificateCheckerErrors.h"
#import "KLCertificateInfo.h"


/**
 * The KLCertificateChecker protocol provides methods for verifying SSL certificates.
 */
@protocol KLCertificateChecker <NSObject>

/**
 * Verifies a SSL certificate on Kaspersky cloud (KSN).
 *
 * @param url A HTTPS site url.
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An object that implements KLCertificateInfo protocol.
 */
- (id<KLCertificateInfo>)certificateInfoForUrl:(NSURL*)url error:(NSError**)errorPtr;

@end
