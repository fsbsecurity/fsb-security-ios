#import <Foundation/Foundation.h>


/**
 * Certificate checker error codes
 */
typedef enum {
    kKLCertificateCheckerErrorCode_Unknown,
    kKLCertificateCheckerErrorCode_InvalidArgument,
    kKLCertificateCheckerErrorCode_ConnectionError,
    kKLCertificateCheckerErrorCode_UnsupportedURLScheme
} KLCertificateCheckerErrorCode;

/**
 * Certificate checker error domain
 */
extern NSString* const kKLCertificateCheckerErrorDomain;