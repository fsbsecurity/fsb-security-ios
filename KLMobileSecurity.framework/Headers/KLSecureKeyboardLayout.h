#import <Foundation/Foundation.h>
#import <UIKit/UIDevice.h>

enum {
    kKLSecureKeyboardLayoutOption_ShowBackspaceButton                               = 1 << 0,
    kKLSecureKeyboardLayoutOption_ShowHidingKeyboardButton                          = 1 << 1,
    kKLSecureKeyboardLayoutOption_ShowReturnButton                                  = 1 << 2,
    kKLSecureKeyboardLayoutOption_ShowShiftButton                                   = 1 << 3,
    kKLSecureKeyboardLayoutOption_ShowSpaceButton                                   = 1 << 4,
    kKLSecureKeyboardLayoutOption_ShowSwitchAlphabetLayoutButton                    = 1 << 5,
    kKLSecureKeyboardLayoutOption_ShowSwitchToNumericLayoutButtonInLastSymbolRow    = 1 << 6,
    kKLSecureKeyboardLayoutOption_ShowSwitchToNumericLayoutButtonInLastRow          = 1 << 7,
    kKLSecureKeyboardLayoutOption_ShowSwitchToSpecSymbolLayoutButton                = 1 << 8,
    kKLSecureKeyboardLayoutOption_ShowSwitchToAlphabetLayoutButton                  = 1 << 9,
};
typedef NSUInteger KLSecureKeyboardLayoutOptions;


/**
 * @abstract Secure keyboard layout
 *
 * @discussion The class is abstract and cannot be subclassed. Use predefined layouts (e.g. [KLSecureKeyboardTheme englishLayout]),
 * load layout from XML file ([KLSecureKeyboardLayout layoutWithContentsOfURL:]),
 * or create standard layout with symbol rows ([KLSecureKeyboardLayout layoutWithSymbolRows:options:error:]).
 */
__attribute__((visibility("default")))
@interface KLSecureKeyboardLayout : NSObject

/**
 * @abstract Create layout by XML layout file.
 *
 * @param url URL to XML layout file.
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return Instance of KLSecureKeyboardLayout subclass in case of success, otherwise nil.
 */
+ (instancetype)layoutWithContentsOfURL:(NSURL *)url error:(NSError *__autoreleasing *)error;

/**
 * @abstract Create standard layout by providing symbols for symbol buttons.
 *
 * @discussion Appearance of keyboard layout can be customized using options mask. Buttons positions and sizes are calculated automatically.
 *
 * @param symbolRows Array of arrays of symbols (NSString objects). n-th array of symbols corresponds to the n-th row of symbol buttons on keyboard.
 * @param options A mask of options indicating which buttons should be present.
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return Instance of KLSecureKeyboardLayout subclass in case of success, otherwise nil.
 */
+ (instancetype)layoutWithSymbolRows:(NSArray *)symbolRows options:(KLSecureKeyboardLayoutOptions)options error:(NSError *__autoreleasing *)error;

/**
 * @abstract Predefined english keyboard layout.
 *
 * @return Instance of KLSecureKeyboardLayout subclass with predefined layout.
 */
+ (instancetype)englishLayout;

/**
 * @abstract Predefined russian keyboard layout.
 *
 * @return Instance of KLSecureKeyboardLayout subclass with predefined layout.
 */
+ (instancetype)russianLayout;

/**
 * @abstract Predefined numeric keyboard layout.
 *
 * @return Instance of KLSecureKeyboardLayout subclass with predefined layout.
 */
+ (instancetype)numericLayout;

/**
 * @abstract Predefined special symbols keyboard layout.
 *
 * @return Instance of KLSecureKeyboardLayout subclass with predefined layout.
 */
+ (instancetype)specSymbolLayout;

@end
