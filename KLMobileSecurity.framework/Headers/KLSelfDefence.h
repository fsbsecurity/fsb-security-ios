#import "KLSelfDefenceAppSignature.h"
#import "KLSelfDefenceDebugging.h"
#import "KLSelfDefenceSwizzlingDetector.h"
#import "KLSelfDefenceSafeAttribute.h"


/**
 * Self-defence initialization options.
 */
typedef enum {
    /**
     * App signature verification mechanism is activated after calling KLInitSelfDefence() with this option.
     */
    kKLInitSelfDefenceOption_VerifyAppSignature = 1 << 0,
    /**
     * Debugging is denied after calling KLInitSelfDefence() with this option.
     */
    kKLInitSelfDefenceOption_PreventDebugging = 1 << 1,
    /**
     * Method swizzling detection mechanism is activated after calling KLInitSelfDefence() with this option.
     */
    kKLInitSelfDefenceOption_DetectSwizzling = 1 << 2,
} KLInitSelfDefenceOption;


/**
 * Verifies that the application files were not maliciously modified.
 *
 * Should be called from main()
 *
 * @param options Set of flags from KLInitSelfDefenceOption enumeration.
 */
#ifdef __cplusplus
extern "C"
#endif
void KLInitSelfDefence(int options);


/**
 * The KLSelfDefence protocol.
 */
@protocol KLSelfDefence <NSObject>

/*
 * Returns an KLSelfDefenceAppSignature instance which is used for checking app signature.
 */
@property (readonly) id<KLSelfDefenceAppSignature> appSignature;

/*
 * Returns an KLSelfDefenceDebugging instance witch is used for detecting debuggers.
 */
@property (readonly) id<KLSelfDefenceDebugging> debugging;

/*
* Returns an KLSelfDefenceSwizzlingDetector instance witch is used for detecting swizzles.
*/
@property (readonly) id<KLSelfDefenceSwizzlingDetector> swizzlingDetector;

@end
