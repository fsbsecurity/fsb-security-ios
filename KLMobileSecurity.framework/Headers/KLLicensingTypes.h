#import <Foundation/Foundation.h>


typedef void (^KLLicensingSuccessHandler)();
typedef void (^KLLicensingErrorHandler)(NSError* error);
