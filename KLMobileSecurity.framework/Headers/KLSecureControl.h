#import <UIKit/UIControl.h>


__attribute__((visibility("default")))
@interface KLSecureControl : UIControl

/**
 * Background view.
 */
@property (nonatomic, strong) UIView* backgroundView;

@end
