#import <Foundation/Foundation.h>


/**
 * @enum KLSelfDefenceAppSignatureErrorCode
 * @abstract KLSelfDefenceAppSignature error codes
 */
typedef enum {
    kKLSelfDefenceAppSignatureErrorCode_Unknown,
    kKLSelfDefenceAppSignatureErrorCode_InternalError,
    kKLSelfDefenceAppSignatureErrorCode_InvalidAppExecutableFileSignature,
    kKLSelfDefenceAppSignatureErrorCode_InvalidAppResourceFileSignature,
} KLSelfDefenceAppSignatureErrorCode;

/**
 * @const kKLSelfDefenceAppSignatureErrorDomain
 * @abstract KLSelfDefenceAppSignature error domain
 */
extern NSString* const kKLSelfDefenceAppSignatureErrorDomain;

/**
 * @const kKLSelfDefenceAppSignatureErrorFilePathErrorKey
 * @abstract The NSError userInfo dictionary key used to store and retrieve the NSString object for path of the file which signature is invalid.
 */
extern NSString* const kKLSelfDefenceAppSignatureErrorFilePathErrorKey;