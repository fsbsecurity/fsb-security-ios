#import "KLSelfDefenceSwizzledMethod.h"


/**
 * The KLSelfDefenceSwizzlingVerdict enumerator provides the swizzling verdict.
 */
typedef enum {
    kKLSelfDefenceSwizzlingVerdict_Good,
    kKLSelfDefenceSwizzlingVerdict_Suspicious,
    kKLSelfDefenceSwizzlingVerdict_Bad
} KLSelfDefenceSwizzlingVerdict;


/**
 * The protocol provides information about swizzled object.
 */
@protocol KLSelfDefenceSwizzlingInfo <NSObject>

/**
 * Returns the swizzling verdict.
 */
@property (nonatomic, readonly) KLSelfDefenceSwizzlingVerdict verdict;

/**
 * Returns suspicious threat objects.
 */
@property (nonatomic, readonly) NSArray* suspiciousThreats;

/**
 * Returns dangerous threat objects.
 */
@property (nonatomic, readonly) NSArray* dangerousThreats;

@end
