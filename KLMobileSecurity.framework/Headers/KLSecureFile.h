#import "KLSecureFileExceptions.h"

typedef NS_OPTIONS(NSUInteger, KLSecureFileMode) {
    kKLSecureFileMode_Read = 1 << 0,
    kKLSecureFileMode_Write = 1 << 1,
    kKLSecureFileMode_Create = 1 << 2,
    kKLSecureFileMode_Truncate = 1 << 3,
    kKLSecureFileMode_Append = 1 << 4
};

@protocol KLSecureFile <NSObject>

/**
 * Current position within the secure file
 *
 * @throws Exception with kKLSecureFileOperationException name
 */
@property (nonatomic, readonly) NSUInteger offset;

/**
 * Secure file size in bytes
 *
 * @throws Exception with kKLSecureFileOperationException name
 */
@property (nonatomic, readonly) NSUInteger size;

/**
 * Opens secure file. If file is already open if will be closed before the reopening.
 *
 * @param mode File open mode (mask of KLSecureFileMode's)
 *
 * @throws Exception with kKLSecureFileOperationException name
 *
 * @return YES if file was opened otherwise NO
 */
- (BOOL)openWithMode:(NSUInteger)mode;

/**
 * Checks if secure file is open
 *
 * @return YES if secure file is open otherwise NO
 */
- (BOOL)isOpen;

/**
 * Closes secure file. Closing closed file produces no effect
 */
- (void)close;

/**
 * Reads data of the specified length from the current position in secure file
 *
 * @param length Data length
 *
 * @return read data
 *
 * @throws Exception with kKLSecureFileOperationException name
 */
- (NSData *)readDataOfLength:(NSUInteger)length;

/**
 * Reads data from the current position in secure file to the end of file
 *
 * @return read data
 *
 * @throws Exception with kKLSecureFileOperationException name
 */
- (NSData *)readDataToEnd;

/**
 * Writes data to the current file pointer position in secure file
 *
 * @param data Data to write
 *
 * @throws Exception with kKLSecureFileOperationException name
 */
- (void)writeData:(NSData *)data;

/**
 * Moves the file pointer to the specified offset within the secure file
 *
 * @param offset Offset in bytes from the beginning of the file
 *
 * @throws exception with kKLSecureFileOperationException name
 */
- (void)seekToOffset:(NSUInteger)offset;

/**
 * Sets file size
 *
 * @param size File size
 *
 * @throws exception with kKLSecureFileOperationException name
 *
 */
- (void)truncateAtOffset:(NSUInteger)offset;

/**
 * Flushes the file
 *
 * @throws exception with kKLSecureFileOperationException name
 *
 */
- (void)flush;

/**
 * Bind touchID and password.
 * Use [KLSecureDatabaseManager secureFileByTouchIdWithPath: completion:] to get secure secureFile instances by touchID
 *
 * Declarate keychain group 'com.kaspersky.sdk' in your project Entitlements.plist"
 */
- (BOOL)bindTouchId;

/**
 * Unbind touchID and password.
 */
- (void)unBindTouchId;

@end
