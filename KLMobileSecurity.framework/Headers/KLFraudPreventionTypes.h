#import <Foundation/Foundation.h>

typedef void (^KLFraudPreventionSuccessHandler)();
typedef void (^KLFraudPreventionErrorHandler)(NSError *error);
