#import "KLFingerprintDevicePersistent.h"

/**
 * The KLFingerprintAppPersistent protocol provides information about Persistent properties for Application.
 */
@protocol KLFingerprintAppPersistent <KLFingerprintDevicePersistent>

/**
 * Returns a unique id for application
 */
@property (nonatomic, readonly) NSString *persistentAppPropertiesFingerprintId;

// persistent application properies

/**
 * Returns a unique id for vendor. avalible only for ios 6 and greater.
 */
@property (nonatomic, readonly) NSString *idForVendor NS_AVAILABLE_IOS(6_0);


@end