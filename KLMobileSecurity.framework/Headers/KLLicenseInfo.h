#import <Foundation/Foundation.h>


/**
 * The KLLicenseType enumerator provides the types of license.
 */
typedef enum {
    kKLLicenseType_Unknown,
    kKLLicenseType_Beta,
    kKLLicenseType_Trial,
    kKLLicenseType_Test,
    kKLLicenseType_OEM,
    kKLLicenseType_Commercial,
    kKLLicenseType_Subscription,
    kKLLicenseType_SubscriptionProtection
} KLLicenseType;

/**
 * The KLLicenseState enumerator provides the states of license
 */
typedef enum {
    kKLLicenseState_NotBlocked,
    kKLLicenseState_Blocked
} KLLicenseState;

/**
 * The KLLicenseSubscriptionState enumerator provides the states of subscription
 */
typedef enum {
    kKLLicenseSubscriptionState_Unknown,
    kKLLicenseSubscriptionState_Active,
    kKLLicenseSubscriptionState_Paused,
    kKLLicenseSubscriptionState_SoftCancelled,
    kKLLicenseSubscriptionState_HardCancelled
} KLLicenseSubscriptionState;

/**
 * The KLLicenseExpirationFunctionalityLevel enumerator provides level of functionality
 */
typedef enum {
    kKLLicenseExpirationFunctionalityLevel_NoFeatures,
    kKLLicenseExpirationFunctionalityLevel_FunctionWithoutUpdates,
    kKLLicenseExpirationFunctionalityLevel_Limited,
    kKLLicenseExpirationFunctionalityLevel_Full
} KLLicenseExpirationFunctionalityLevel;


/**
 * The KLLicensing protocol provides information about license
 */
@protocol KLLicenseInfo <NSObject>

/**
 * A type of license
 */
@property (nonatomic, readonly) KLLicenseType type;

/**
 * A state of license
 */
@property (nonatomic, readonly) KLLicenseState state;

/**
 * A state of subscription, valid if license type is kKLLicenseType_Subscription
 */
@property (nonatomic, readonly) KLLicenseSubscriptionState subscriptionState;

/**
 * An activation code
 */
@property (nonatomic, readonly) NSString* activationCode;

/**
 * A serial number
 */
@property (nonatomic, readonly) NSString* serialNumber;

/**
 * A header of ticket
 */
@property (nonatomic, readonly) NSString* ticketHeader;

/**
 * A product name
 */
@property (nonatomic, readonly) NSString* productName;

/**
 * A product ID
 */
@property (nonatomic, readonly) NSInteger productId;

/**
 * A product version
 */
@property (nonatomic, readonly) NSString* productVersion;

/**
 * Application ID
 */
@property (nonatomic, readonly) NSInteger appId;

/**
 * A ticket data
 */
@property (nonatomic, readonly) NSData* ticketData;

/**
 * A date of creation
 */
@property (nonatomic, readonly) NSDate* creationDate;

/**
 * A date of start using
 */
@property (nonatomic, readonly) NSDate* startUsageDate;

/**
 * A date of expiration
 */
@property (nonatomic, readonly) NSDate* expirationDate;

/**
 * A date of last ticket update
 */
@property (nonatomic, readonly) NSDate* lastUpdateDate;

/**
 * A date of ticket expiration
 */
@property (nonatomic, readonly) NSDate* ticketExpirationDate;

/**
 * A tolerance period (days) defines how many days application must remain fully functional after subscription license expiration
 */
@property (nonatomic, readonly) NSUInteger subscriptionGracePeriod;

/**
 * If subscriptionEndDate is nil the subscription is unlimited, otherwise - limited
 */
@property (nonatomic, readonly) NSDate* subscriptionEndDate;

/**
 * Provides level of functionality after ticket expiration
 */
@property (nonatomic, readonly) KLLicenseExpirationFunctionalityLevel ticketExpirationFunctionality;

/**
 * Provides level of functionality after license expiration
 */
@property (nonatomic, readonly) KLLicenseExpirationFunctionalityLevel licenseExpirationFunctionality;

/**
 * A number of licensed devices
 */
@property (nonatomic, readonly) NSUInteger deviceCount;

/**
 * A validity period (days)
 */
@property (nonatomic, readonly) NSUInteger validityPeriod;

/**
 * Returns YES if it has ticket data
 */
@property (nonatomic, readonly) BOOL hasTicket;

/**
 * Returns YES if license is active
 */
@property (nonatomic, readonly) BOOL isActive;

/**
 * Returns YES if license is expired
 */
@property (nonatomic, readonly) BOOL isExpired;

/**
 * Returns YES if license is blocked
 */
@property (nonatomic, readonly) BOOL isBlocked;

/**
 * Returns YES if ticket is expired
 */
@property (nonatomic, readonly) BOOL isTicketExpired;

@end