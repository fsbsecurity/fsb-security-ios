#import <Foundation/Foundation.h>
#import "KLIPAddress.h"
#import "KLFingerprintLocator.h"

/**
 * The KLFingerprintDeviceNotPersistent protocol provides information about Not Persistent properties for Device.
 */
@protocol KLFingerprintDeviceNotPersistent <NSObject>

/**
 * Returns a unique id for device (not guaranteed that two different devices have two different notPersistentDevicePropertiesFingerprintId).
 */
@property (nonatomic, readonly) NSString* notPersistentDevicePropertiesFingerprintId;

// not persistent device properties

/**
 * Returns a device name
 */
@property (nonatomic, readonly) NSString* name;

/**
 * Returns a system name
 */
@property (nonatomic, readonly) NSString* systemName;

/**
 * Returns a locale
 */
@property (nonatomic, readonly) NSLocale* locale;

/**
 * Returns a operation system version
 */
@property (nonatomic, readonly) NSString* OSVersion;

/**
 * Returns a timestamp
 */
@property (nonatomic, readonly) NSTimeInterval timestamp;

/**
 * Returns a ip Address
 */
@property (nonatomic, readonly) KLIPAddress* ipAddress;

/**
 * Returns a carrier name
 */
@property (nonatomic, readonly) NSString* carrierName;

/**
 * Returns a mobile country code
 */
@property (nonatomic, readonly) NSString* mobileCountryCode;

/**
 * Returns a mobile network code
 */
@property (nonatomic, readonly) NSString* mobileNetworkCode;

/**
 * Factory method for getting locator with specified accuracy and duration
 *
 * @param desiredLocationAccuracy: Desired location detection accuracy (value of CLLocation.horizontalAccuracy).
 * @param maxLocationDetectionDuration: Maximum duration of location detection.
 */
-(id<KLFingerprintLocator>)locatorWithAccuracy:(CLLocationAccuracy)desiredLocationAccuracy duration:(NSTimeInterval)maxLocationDetectionDuration;



@end
