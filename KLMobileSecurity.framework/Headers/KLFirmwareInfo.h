#import <Foundation/Foundation.h>

/**
 * The KLFirmwareVerdict enumerator provides the information about the verdict of firmare.
 */
typedef enum {
    kKLFirmwareVerdict_Unknown, // Unknown firmware
    kKLFirmwareVerdict_Good,    // Good firmware
    kKLFirmwareVerdict_Bad      // Customized firmware
} KLFirmwareVerdict;


/**
 * The KLFirmwareInfo protocol provides methods for firmware verification
 */
@protocol KLFirmwareInfo <NSObject>

/**
 * Returns a firmware verification verdict
 */
@property (nonatomic, readonly) KLFirmwareVerdict verdict;

/**
 * Returns TRUE if firmware is up-to-date
 */
@property (nonatomic, readonly) BOOL isUpToDate;

/**
 * Returns latest available firmware version
 */
@property (nonatomic, readonly) NSString* upToDateVersion;

@end
