#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>

/**
 * Parts of alternative symbols panel
 */
typedef NS_ENUM(NSUInteger, KLSecureKeyboardAltPanelImageParts) {
    kKLSecureKeyboardAltPanelImagePart_BottomCenter,
    kKLSecureKeyboardAltPanelImagePart_TopLeft,
    kKLSecureKeyboardAltPanelImagePart_TopCenterLeft,
    kKLSecureKeyboardAltPanelImagePart_TopCenterRight,
    kKLSecureKeyboardAltPanelImagePart_TopRight
};


/**
 * Secure keyboard theme
 *
 * The class is abstract. Use predefined themes (e.g. [KLSecureKeyboardTheme lightKeyboardTheme]).
 */
__attribute__((visibility("default")))
@interface KLSecureKeyboardTheme : NSObject

@property (nonatomic, readonly) UIColor* keyboardBackgroundColor;
@property (nonatomic, readonly) UIImage* keyboardBackgroundImage;
@property (nonatomic, readonly) UIImage* buttonLightBackgroundImage;
@property (nonatomic, readonly) UIImage* buttonDarkBackgroundImage;
@property (nonatomic, readonly) UIImage* shiftButtonCapsLockBackgroundImage;
@property (nonatomic, readonly) UIImage* backspaceButtonImage;
@property (nonatomic, readonly) UIImage* backspaceButtonSmallImage;
@property (nonatomic, readonly) UIImage* hideKeyboardButtonImage;
@property (nonatomic, readonly) UIImage* hideKeyboardButtonSmallImage;
@property (nonatomic, readonly) UIImage* shiftButtonImage;
@property (nonatomic, readonly) UIImage* shiftButtonSmallImage;
@property (nonatomic, readonly) UIImage* shiftButtonSelectedImage;
@property (nonatomic, readonly) UIImage* shiftButtonSelectedSmallImage;
@property (nonatomic, readonly) UIImage* shiftButtonCapsLockImage;
@property (nonatomic, readonly) UIImage* shiftButtonCapsLockSmallImage;
@property (nonatomic, readonly) UIImage* switchAlphabetLayoutButtonImage;
@property (nonatomic, readonly) UIImage* switchAlphabetLayoutButtonSmallImage;
@property (nonatomic, readonly) NSArray* alternativeSymbolsPanelImageParts;

/**
 * @return Predefined light keyboard theme.
 */
+ (instancetype)lightKeyboardTheme;

/**
 * @return Predefined dark keyboard theme.
 */
+ (instancetype)darkKeyboardTheme;

/**
 * Designated constructor.
 */
- (instancetype)init;

@end
