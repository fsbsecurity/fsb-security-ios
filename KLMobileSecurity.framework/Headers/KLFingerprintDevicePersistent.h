#import <UIKit/UIKit.h>

/**
 * The KLFingerprintDevicePersistent protocol provides information about Persistent properties for Device.
 */
@protocol KLFingerprintDevicePersistent <NSObject>

/**
 * Returns a unique id for device (guaranteed that two different devices have two different
 * persistentDevicePropertiesFingerprintId).
 */
@property (nonatomic, readonly) NSString *persistentDevicePropertiesFingerprintId;

// persistent device properties

/**
 * Returns a device model.
 */
@property (nonatomic, readonly) NSString *deviceModel;

/**
 * Returns a device identifier.
 */
@property (nonatomic, readonly) NSString *deviceId;

/**
 * Returns a macAddress.
 * The method is deprecated.
 */
@property (nonatomic, readonly) NSString *macAddress NS_DEPRECATED_IOS(5_0, 7_0);

/**
 * Returns a device screen size in points
 */
@property (nonatomic, readonly) CGSize deviceScreenSizeInPoints;

/**
 * Returns a device screen size in pixels.
 * If deviceScreenSizeInPoints == 320 * 480 then deviceScreenSizeInPixels will be 640 * 960 for retina display,
 * and 320 * 480 for not retina.
 */
@property (nonatomic, readonly) CGSize deviceScreenSizeInPixels;

/**
 * Return a operation system id as UUID.
 */
@property (nonatomic, readonly) NSString *operationSystemId;

/**
 * Return a simulator or device.
 */
@property (nonatomic, readonly) bool simulator;

@end