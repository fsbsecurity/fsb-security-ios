#import "KLFingerprintAppNotPersistent.h"
#import "KLFingerprintAppPersistent.h"

/**
 * The KLFingerprint protocol provides information about device.
 */
@protocol KLFingerprint <KLFingerprintAppPersistent, KLFingerprintAppNotPersistent>

/**
 * Returns a unique id for fingerprint.
 */
@property (nonatomic, readonly) NSString *fingerprintId;

/**
 * Returns a current KasperskySDK version.
 */
@property (nonatomic, readonly) NSString *sdkVersion;

@end
