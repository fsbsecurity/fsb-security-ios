#import <Foundation/Foundation.h>


/**
 * The class provides Kaspersky SDK parameters.
 */
__attribute__((visibility("default")))
@interface KLConfiguration : NSObject

/**
 * Creates an Kaspersky SDK configuration object.
 *
 * @return An KLConfiguration instance.
 */
+ (instancetype)configuration;

/**
 * Turn On debugging configuration.
 * By default is NO.
 */
@property (nonatomic, assign, getter = isDebugging) BOOL debugging;

/**
 * The path to SDK working directory.
 * By default is App Documents direcotry.
 */
@property (nonatomic, copy) NSString* dataDirectory;

@end
