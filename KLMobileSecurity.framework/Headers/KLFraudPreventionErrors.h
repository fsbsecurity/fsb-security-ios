#import <Foundation/Foundation.h>

/**
 * Fraud prevention error codes
 */
typedef enum {
    kKLFraudPreventionErrorCode_Unknown,
    kKLFraudPreventionErrorCode_InvalidArgument,
    kKLFraudPreventionErrorCode_NotConfigured
} KLFraudPreventionErrorCode;

/**
 * Fraud prevention error domain
 */
extern NSString *const kKLFraudPreventionErrorDomain;