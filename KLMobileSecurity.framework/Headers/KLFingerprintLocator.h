#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^KLFingerprintLocationSuccessHandler)(CLLocation* location);
typedef void (^KLFingerprintLocationErrorHandler)(NSError* error);

@protocol KLFingerprintLocator <NSObject>

/**
 * Desired location detection accuracy (value of CLLocation.horizontalAccuracy).
 */
@property (nonatomic, readonly) CLLocationAccuracy desiredLocationAccuracy;

/**
 * Maximum duration of location detection.
 */
@property (nonatomic, readonly) NSTimeInterval maxLocationDetectionDuration;


/**
 * Asynchronously get device current location (only one update operation simultaneously)
 *
 * The method is waiting the location to be detected with desiredLocationAccuracy accuracy
 * with maxLocationDetectionDuration seconds timeout.
 *
 * @param successHandler: If update current location ended with success,
 * successHandler called, its parameters contains pointer to CLLocation object
 * @param failHandler: If an error occurs, failHandler called, its parameters
 * contains an NSError object that describes the problem.
 */
- (void)currentLocationWithSuccessHandler:(KLFingerprintLocationSuccessHandler)successHandler
                             errorHandler:(KLFingerprintLocationErrorHandler)errorHandler;

@end
