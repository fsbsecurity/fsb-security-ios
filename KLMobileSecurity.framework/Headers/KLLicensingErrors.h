#import <Foundation/Foundation.h>


/**
 * Licensing error codes
 */
typedef enum {
    kKLLicensingErrorCode_Unknown,
    kKLLicensingErrorCode_InvalidArgument,
    kKLLicensingErrorCode_AlreadyActivated,
    kKLLicensingErrorCode_LicenseExpired,
    kKLLicensingErrorCode_LicenseBlocked,
    kKLLicensingErrorCode_InvalidLicenseState,
    kKLLicensingErrorCode_SystemError,
    kKLLicensingErrorCode_ConnectionError,
    kKLLicensingErrorCode_LogicError,
    kKLLicensingErrorCode_ServerError,
    kKLLicensingErrorCode_LicenseNotFound,
    kKLLicensingErrorCode_BadRequest,
    kKLLicensingErrorCode_ActivationCodeBlocked,
    kKLLicensingErrorCode_WrongActivationCode,
    kKLLicensingErrorCode_WrongApplicationId,
    kKLLicensingErrorCode_ActivationPeriodExpired,
    kKLLicensingErrorCode_ActivationAttemptsExhausted,
    kKLLicensingErrorCode_ActivationCodeAlreadyInUse,
    kKLLicensingErrorCode_RegionMismatch,
    kKLLicensingErrorCode_InvalidResponse,
    kKLLicensingErrorCode_TicketNotFound
} KLLicensingErrorCode;

/**
 * Licensing error domain
 */
extern NSString* const kKLLicensingErrorDomain;