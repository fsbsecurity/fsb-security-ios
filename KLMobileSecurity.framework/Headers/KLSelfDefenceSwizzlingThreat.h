#import <Foundation/Foundation.h>


/**
 * The protocol provides information about swizzling threat.
 */
@protocol KLSelfDefenceSwizzlingThreat <NSObject>

/**
 * Returns the target object address.
 */
@property (nonatomic, readonly) void* address;

/**
 * Returns the image name of target object.
 */
@property (nonatomic, readonly) NSString* image;

@end
