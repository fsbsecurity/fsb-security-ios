#import <Foundation/Foundation.h>


/**
 * The KLWiFiVerdict enumerator provides the Wi-Fi verdicts.
 */
typedef enum {
    kKLWiFiVerdict_Unknown,
    kKLWiFiVerdict_Safe,
    kKLWiFiVerdict_Unsafe
} KLWiFiVerdict;


/**
 * The protocol provides information about the safety of the currently used Wi-Fi access point.
 */
@protocol KLWiFiInfo <NSObject>

/**
 * Returns the Wi-Fi analyze verdict.
 */
@property (nonatomic, readonly) KLWiFiVerdict verdict;

@end
