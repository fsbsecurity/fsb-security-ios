#import <Foundation/Foundation.h>

/**
 * The KLSDKState enumerator provides the all available SDK states
 */
typedef enum {
    // The SDK is not activated.
    // To activate license call method activateWithSuccessHandler:errorHandler: of KLLicensing protocol
    kKLSDKState_NotActivated,
    // No valid license.
    // The current license is expired or blocked.
    // For more details about current license see to the KLLicenseInfo protocol
    kKLSDKState_NoLicense,
    // The grace period to provide user identifier is expired.
    // For per-user license model after activation you must provide user identifier within the grace period.
    // Usually the grace period is 24 hour.
    // To provide user identifier call method sendUserId:successHandler:errorHandler: of KLPerUserLicensing protocol
    kKLSDKState_GracePeriodExpired,
    // The SDK is ready to use.
    kKLSDKState_ReadyToUse
} KLSDKState;