#import "KLX509Certificate.h"


@protocol KLSecureConnection;

/**
 * The KLSecureConnectionDelegate protocol describes methods that should be implemented by the delegate for an instance of KLSecureConnection.
 */
@protocol KLSecureConnectionDelegate <NSObject>
@optional

/**
 * Sent when a connection fails to load its request successfully.
 *
 * @param connection The connection sending the message.
 * @param error An error object containing details of why the connection failed to load the request successfully.
 */
- (void)secureConnection:(id<KLSecureConnection>)connection didFailWithError:(NSError*)error;

/**
 * Sent to determine whether the URL loader should consult the credential storage for authenticating the connection.
 *
 * @param connection The connection sending the message.
 * @return By returning NO, the delegate tells the connection not to consult the credential storage and makes itself responsible for providing credentials for any authentication challenges.
 */
- (BOOL)secureConnectionShouldUseCredentialStorage:(id<KLSecureConnection>)connection;

/*
 * Tells the delegate that the connection will send a request for an authentication challenge.
 *
 * @param connection The connection sending the message.
 * @param challenge The authentication challenge for which a request is being sent.
 */
- (void)secureConnection:(id<KLSecureConnection>)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge*)challenge;

/*
 * Sent to determine whether when server used unknown or self signed certificates.
 *
 * @param connection The connection sending the message.
 * @param certificate The information about X509 certificate
 * @return YES if certificate should be accepted. By default return NO.
 */
- (BOOL)secureConnection:(id<KLSecureConnection>)connection shouldAcceptUnknownCertificate:(id<KLX509Certificate>)certificate;

@end
