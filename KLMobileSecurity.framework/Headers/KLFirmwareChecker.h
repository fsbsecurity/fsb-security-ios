#import "KLFirmwareCheckerErrors.h"
#import "KLFirmwareInfo.h"


/**
 * The protocol provides methods for firmware verification
 */
@protocol KLFirmwareChecker <NSObject>

/**
 * Receives information about device firmare
 *
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An object which provides information about firmware
 */
- (id<KLFirmwareInfo>)firmwareInfoWithError:(NSError* __autoreleasing*)errorPtr;

@end
