#import "KLSelfDefenceSwizzlingThreat.h"
#import <objc/objc.h>
#import <objc/runtime.h>


/**
 * The KLSelfDefenceSwizzlingMethodType enumerator provides the type of the method.
 */
typedef enum {
    kKLSelfDefenceSwizzlingMethodType_InstanceMethod,
    kKLSelfDefenceSwizzlingMethodType_ClassMethod
} KLSelfDefenceSwizzlingMethodType;


/**
 * The protocol provides information about swizzled method.
 */
@protocol KLSelfDefenceSwizzledMethod <KLSelfDefenceSwizzlingThreat>

/**
 * Returns the swizzled method.
 */
@property (nonatomic, readonly) Method method;

/**
 * Returns the swizzled method type.
 */
@property (nonatomic, readonly) KLSelfDefenceSwizzlingMethodType methodType;

/**
 * Returns the class of swizzled method.
 */
@property (nonatomic, readonly) Class cls;

/**
 * Returns the image name of class of swizzled method.
 */
@property (nonatomic, readonly) NSString* classImage;

@end