#import <Foundation/Foundation.h>
#import "KLUrlInfo.h"

/**
 * The KLUrlVerdict enumerator provides the verdict, whether the URL provides any threats. The verdict is obtained from KSN.
 */
typedef enum {
    kKLUrlVerdict_Unknown, // Unknown url
    kKLUrlVerdict_Good,    // Good url
    kKLUrlVerdict_Bad      // Bad url
} KLUrlVerdict;


/**
 * The protocol provides information about result of check url operation.
 */
@protocol KLUrlCheckResult <NSObject, NSCoding>

/**
 * Returns the URL verification verdict.
 */
@property (nonatomic, readonly, assign) KLUrlVerdict verdict;

/**
 * Returns the URL info
 */
@property (nonatomic, readonly, strong) id <KLUrlInfo> urlInfo;

@end