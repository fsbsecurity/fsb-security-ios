#import <Foundation/Foundation.h>

typedef enum {
    kKLUpdateSuccessResult_Updated,
    kKLUpdateSuccessResult_NoUpdatesFound,
    kKLUpdateSuccessResult_NotAllComponentsUpdated
} KLUpdateSuccessResult;

typedef void (^KLUpdaterSuccessHandler)();
typedef void (^KLUpdaterSuccessResultHandler)(KLUpdateSuccessResult result);
typedef void (^KLUpdaterErrorHandler)(NSError* error);