#import <UIKit/UIView.h>
#import <UIKit/UITextInput.h>
#import "KLSecureKeyboardLayout.h"
#import "KLSecureKeyboardTheme.h"
#import "KLSecureKeyboardErrors.h"


/**
 * @abstract Secure keyboard
 *
 * @discussion Use [KLSecureKeyboard keyboardWithAlphabetLayouts:numericLayout:specSymbolLayout:theme:] to get secure Keyboard instances.
 *             The class is abstract and cannot be subclassed.
 */
__attribute__((visibility("default")))
@interface KLSecureKeyboard : UIView

/**
 * @abstract Text input responder which receives messages from the keyboard.
 *
 * @discussion KLSecureKeyboard manages this property automatically. For UITextField and UITextView a good practice is to set and release this property in delegate methods.
 */
@property (nonatomic, weak) UIResponder<UITextInput>* textInput;

/**
 * @abstract Alphabet keyboard layouts.
 */
@property (nonatomic, readonly) NSArray* alphabetLayouts;

/**
 * @abstract Numeric keyboard layout.
 */
@property (nonatomic, readonly) KLSecureKeyboardLayout* numericLayout;

/**
 * @abstract Special symbols keyboard layout.
 */
@property (nonatomic, readonly) KLSecureKeyboardLayout* specSymbolLayout;

/**
 * @abstract Current keyboard layout.
 */
@property (nonatomic, readonly) KLSecureKeyboardLayout* currentLayout;

/**
 * @abstract A boolean value indicating whether the keyboard shuffles symbol buttons after presenging keyboard.
 *
 * @discussion The default value is NO.
 */
@property (nonatomic, assign) BOOL shufflesSymbolButtons;

/**
 * @abstract Creates secure keyboard view. If there is no database at the path new database will be created
 *
 * @param alphabetLayouts Alphabet keyboard layouts (array of KLSecureKeyboardLayout objects).
 * @param numericLayout Numeric keyboard layouts (is used by `switch-numeric-layout` button).
 * @param specSymbolLayout Special symbols keyboard layout  (is used by `switch-spec-symbol-layout` button).
 * @param theme Keyboard theme.
 *
 * @return Instance of KLSecureKeyboard subclass in case of success, otherwise nil.
 */
+ (instancetype)keyboardWithAlphabetLayouts:(NSArray *)alphabetLayouts
                              numericLayout:(KLSecureKeyboardLayout *)numericLayout
                           specSymbolLayout:(KLSecureKeyboardLayout *)specSymbolLayout
                                      theme:(KLSecureKeyboardTheme *)theme;

/**
 * @abstract Switch alphabet keyboard layout.
 *
 * @param layoutIndex Index of the layout in alphabetLayouts array.
 */
- (void)switchToAlphabetLayoutAtIndex:(NSUInteger)layoutIndex;

/**
 * @abstract Switch latest alphabet keyboard layout.
 */
- (void)switchToLatestAlphabetLayout;

/**
 * @abstract Switch to numeric keyboard layout.
 */
- (void)switchToNumericLayout;

/**
 * @abstract Switch to special symbols keyboard layout.
 */
- (void)switchToSpecSymbolLayout;

@end
