#import "KLUpdaterErrors.h"
#import "KLUpdaterTypes.h"


/**
 * The protocol provides methods for updating Kaspersky SDK data files.
 */
@protocol KLUpdater <NSObject>

/**
 * The date of last successful update
 */
@property (nonatomic, readonly) NSDate* lastUpdateDate;

/**
 * Download and update SDK's data files in separate background thread.
 *
 * @param successHandler A block object which is to be executed when update is finished with success.
 * @param errorHandler A block object which is to be executed when update is failed with error
 * (passes NSError object as parameter of failCompletionHandler).
 */
- (void)updateAsyncWithSuccessHandler:(KLUpdaterSuccessHandler)successHandler
                         errorHandler:(KLUpdaterErrorHandler)errorHandler;

/**
 * Download and update SDK's data files in separate background thread.
 *
 * @param successResultHandler A block object which is to be executed when update is finished with success.
 * @param errorHandler A block object which is to be executed when update is failed with error
 * (passes NSError object as parameter of failCompletionHandler).
 */
- (void)updateAsyncWithSuccessResultHandler:(KLUpdaterSuccessResultHandler)successResultHandler
                               errorHandler:(KLUpdaterErrorHandler)errorHandler;

@end
