#import <Foundation/Foundation.h>


@protocol KLSecureConnection;

/**
 * The KLSecureConnectionDataDelegate protocol describes methods that should be implemented by the delegate for an instance of KLSecureConnection.
 */
@protocol KLSecureConnectionDataDelegate <NSObject>
@optional

/**
 * Sent when the connection determines that it must change URLs in order to continue loading a request.
 *
 * @param connection The connection sending the message.
 * @param request The proposed redirected request. The delegate should inspect the redirected request to verify that it meets its needs, and create a copy with new attributes to return to the connection if necessary.
 * @param response The URL response that caused the redirect. May be nil in cases where this method is not being sent as a result of involving the delegate in redirect processing.
 * @return The actual URL request to use in light of the redirection response. The delegate may return request unmodified to allow the redirect, return a new request, or return nil to reject the redirect and continue processing the connection.
 */
- (NSURLRequest*)secureConnection:(id<KLSecureConnection>)connection willSendRequest:(NSURLRequest*)request redirectResponse:(NSURLResponse*)response;

/**
 * Sent when the connection has received sufficient data to construct the URL response for its request.
 *
 * @param connection The connection sending the message.
 * @param response The URL response for the connection's request. This object is immutable and will not be modified by the URL loading system once it is presented to the delegate.
 */
- (void)secureConnection:(id<KLSecureConnection>)connection didReceiveResponse:(NSURLResponse*)response;

/**
 * Sent as a connection loads data incrementally.
 *
 * @param connection The connection sending the message.
 * @param data The newly available data. The delegate should concatenate the contents of each data object delivered to build up the complete data for a URL load.
 */
- (void)secureConnection:(id<KLSecureConnection>)connection didReceiveData:(NSData*)data;

/**
 * Called when an NSURLConnection needs to retransmit a request that has a body stream to provide a new, unopened stream.
 *
 * @param connection The connection sending the message.
 * @param request The url request.
 * @return This delegate method should return a new, unopened stream that provides the body contents for the request.
 */
- (NSInputStream*)secureConnection:(id<KLSecureConnection>)connection needNewBodyStream:(NSURLRequest*)request;

/**
 * Sent as the body (message data) of a request is transmitted (such as in an http POST request).
 *
 * @param connection The connection sending the message.
 * @param bytesWritten The number of bytes written in the latest write.
 * @param totalBytesWritten The total number of bytes written for this connection.
 * @param totalBytesExpectedToWrite The number of bytes the connection expects to write.
 */
- (void)secureConnection:(id<KLSecureConnection>)connection didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite;

/**
 * Sent before the connection stores a cached response in the cache, to give the delegate an opportunity to alter it.
 *
 * @param connection The connection sending the message.
 * @param cachedResponse The proposed cached response to store in the cache.
 * @return The actual cached response to store in the cache. The delegate may return cachedResponse unmodified, return a modified cached response, or return nil if no cached response should be stored for the connection.
 */
- (NSCachedURLResponse*)secureConnection:(id<KLSecureConnection>)connection willCacheResponse:(NSCachedURLResponse*)cachedResponse;

/**
 * Sent when a connection has finished loading successfully.
 *
 * @param connection The connection sending the message.
 */
- (void)secureConnectionDidFinishLoading:(id<KLSecureConnection>)connection;

@end
