#import <Foundation/Foundation.h>


/**
 * Updater error codes
 */
typedef enum {
    kKLUpdaterErrorCode_Unknown,
    kKLUpdaterErrorCode_ConnectionError
} KLUpdaterErrorCode;

/**
 * Updater error domain
 */
extern NSString* const kKLUpdaterErrorDomain;