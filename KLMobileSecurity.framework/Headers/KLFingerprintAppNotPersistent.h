#import "KLFingerprintDeviceNotPersistent.h"

/**
 * The KLFingerprintAppNotPersistent protocol provides information about Not Persistent properties for Application.
 */
@protocol KLFingerprintAppNotPersistent <KLFingerprintDeviceNotPersistent>

/**
 * Returns a unique id for device (not guaranteed that two different devices have two different notPersistentAppPropertiesFingerprintId).
 */
@property (nonatomic, readonly) NSString* notPersistentAppPropertiesFingerprintId;

@end
