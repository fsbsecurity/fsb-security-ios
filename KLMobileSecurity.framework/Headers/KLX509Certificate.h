#import <Foundation/Foundation.h>


/**
 * The KLX509Certificate protocol provides a X509 certificate information.
 */
@protocol KLX509Certificate <NSObject>

/**
 * Returns the name of the certificate authority that issued the certificate.
 */
@property (nonatomic, readonly) NSString* issuerName;

/**
 * Returns the subject distinguished name from the certificate.
 */
@property (nonatomic, readonly) NSString* subjectName;

/**
 * Returns the expiration date from the certificate.
 */
@property (nonatomic, readonly) NSDate* expirationDate;

@end
