#import <Foundation/Foundation.h>

@protocol KLSelfDefenceAppSignature;


/**
 * Self-defence app signature observer protocol
 */
@protocol KLSelfDefenceAppSignatureObserver <NSObject>

@optional

/**
 * Called when app signature verification error occures.
 *
 * @param appSignature an KLSelfDefenceAppSignature instance.
 * @param error NSError object that describes the problem.
 */
- (void)appSignature:(id<KLSelfDefenceAppSignature>)appSignature verificationFailedWithError:(NSError*)error;

/**
 * Called when app file (executable or resource file) signature is verified.
 *
 * @param appSignature an KLSelfDefenceAppSignature instance.
 * @param filePath Path to file.
 */
- (void)appSignature:(id<KLSelfDefenceAppSignature>)appSignature verificationSucceedForFile:(NSString*)filePath;

@end
