#import "KLSelfDefenceSwizzlingInfo.h"
#import "KLSelfDefenceSwizzlingDetectorObserver.h"


/**
 * The KLSelfDefenceSwizzlingDetector protocol provides methods for detecting swizzling attacks.
 */
@protocol KLSelfDefenceSwizzlingDetector <NSObject>

/**
 * The Method verifies that the class has not been swizzled.
 *
 * @param cls The class to verify
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An object which provides the swizzling information about object.
 */
- (id<KLSelfDefenceSwizzlingInfo>)verifyClass:(Class)cls error:(NSError* __autoreleasing *)errorPtr;

/**
 * Adds an observer
 *
 * @param observer Observer to be added
 */
- (void)addObserver:(id<KLSelfDefenceSwizzlingDetectorObserver>)observer;

/**
 * Removes an observer
 *
 * @param observer Observer to be removed
 */
- (void)removeObserver:(id<KLSelfDefenceSwizzlingDetectorObserver>)observer;

@end