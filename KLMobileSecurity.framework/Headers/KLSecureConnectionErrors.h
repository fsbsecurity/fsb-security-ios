#import <Foundation/Foundation.h>


/**
 * Secure connection error codes
 */
typedef enum {
    kKLSecureConnectionErrorCode_Unknown,
    kKLSecureConnectionErrorCode_BadUrl,
    kKLSecureConnectionErrorCode_UnsupportedUrlScheme,
    kKLSecureConnectionErrorCode_PhishingUrl,
    kKLSecureConnectionErrorCode_MalwareUrl,
    kKLSecureConnectionErrorCode_DnsSpoofed,
    kKLSecureConnectionErrorCode_UntrustedCertificate,
    kKLSecureConnectionErrorCode_UnknownCertificate
} KLSecureConnectionErrorCode;

/**
 * Secure connection error domain
 */
extern NSString* const kKLSecureConnectionErrorDomain;
