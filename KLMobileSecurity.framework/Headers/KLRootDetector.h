#import <Foundation/Foundation.h>


/**
 * The protocol provides methods for detecting whether the device has been rooted.
 */
@protocol KLRootDetector <NSObject>

/**
 * Checks the device permissions.
 *
 * @return Returns YES if device has been rooted.
 */
- (BOOL)isDeviceRooted;

@end
