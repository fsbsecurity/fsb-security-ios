#import <Foundation/Foundation.h>


/**
 * Protocol-attribute that helps to detect swizzling.
 * If class inherits this protocol any modification of the class will be detected as critical threat.
 */
@protocol KLSelfDefenceSafeAttribute

@end
