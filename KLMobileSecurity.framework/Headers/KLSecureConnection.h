#import "KLSecureConnectionErrors.h"
#import "KLSecureConnectionDelegate.h"
#import "KLSecureConnectionDataDelegate.h"


/**
 * An KLSecureConnection protocol provides methods to perform the loading of a URL request.
 */
@protocol KLSecureConnection <NSObject>

/**
 * The object that acts as the delegate of the connection.
 */
@property (nonatomic, weak) id<KLSecureConnectionDelegate> delegate;

/**
 * The object that acts as the data delegate of the connection.
 */
@property (nonatomic, weak) id<KLSecureConnectionDataDelegate> dataDelegate;

/**
 * Returns a deep copy of the original connection request.
 */
@property (nonatomic, readonly) NSURLRequest* originalRequest;

/**
 * Returns the current connection request.
 */
@property (nonatomic, readonly) NSURLRequest* currentRequest;

/**
 * Returns the request that real used to connection.
 */
@property (nonatomic, readonly) NSURLRequest* routingRequest;

/**
 * Causes the connection to begin loading data, if it has not already.
 */
- (void)start;

/**
 * Cancels an asynchronous load of a request.
 */
- (void)cancel;

/**
 * Determines the run loop and mode that the connection uses to send messages to its delegate.
 *
 * @param runLoop The NSRunloop instance to use for delegate messages.
 * @param mode The mode in which to supply delegate messages. You may specify custom modes or use one of the modes listed in https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSRunLoop_Class/Reference/Reference.html#//apple_ref/doc/constant_group/Run_Loop_Modes
 */
- (void)scheduleInRunLoop:(NSRunLoop*)runLoop forMode:(NSString*)mode;

/**
 * Causes the connection to stop sending delegate messages using the specified run loop and mode.
 *
 * @param runLoop The run loop instance to unschedule.
 * @param mode The mode to unschedule.
 */
- (void)unscheduleFromRunLoop:(NSRunLoop*)runLoop forMode:(NSString*)mode;

/**
 * Determines the operation queue that is used to send messages to the connection’s delegate.
 *
 * @param queue The operation queue to use for delegate messages.
 */
- (void)setDelegateQueue:(NSOperationQueue*)queue;

@end
