#import <Foundation/Foundation.h>

/**
 * The class provides fraud prevention consumer parameters.
 */
__attribute__((visibility("default")))
@interface KLFraudPreventionConsumer : NSObject

/**
 * Consumer identificator
 */
@property (nonatomic, copy) NSString *identificator;
/**
 * Consumer url
 */
@property (nonatomic, copy) NSString *url;
/**
 * Consumer certificate data
 */
@property (nonatomic, copy) NSData *certificateData;

@end
