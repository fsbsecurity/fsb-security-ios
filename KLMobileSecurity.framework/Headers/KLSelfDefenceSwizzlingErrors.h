#import <Foundation/Foundation.h>


/**
 * Self defence swizzling error codes
 */
typedef enum {
    kKLSelfDefenceSwizzlingErrorCode_Unknown,
    kKLSelfDefenceSwizzlingErrorCode_NotInitialized
} KLSelfDefenceSwizzlingErrorCode;

/**
 * Self defence swizzling error domain
 */
extern NSString* const kKLSelfDefenceSwizzlingErrorDomain;