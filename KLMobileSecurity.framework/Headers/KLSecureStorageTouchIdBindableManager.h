#import "KLSecureDatabase.h"
#import "KLSecureFile.h"
#import "KLSecureStorageManager.h"
#import <LocalAuthentication/LAContext.h>

@protocol KLSecureStorageTouchIdBindableManager <NSObject>

/**
 * Returns secure database with the passed path. Note that this method doesn't open any file. Get password from
 * secure storage by touch id.
 *
 * @param path Secure database file path (UTF-8)
 * @param completion return KLSecureDatabase
 * @param enterPasswordFallbackHandler
 * @param cancellationHandler return cancellation information.
 *
 * Declarate keychain group 'com.kaspersky.sdk' in your project Entitlements.plist"
 */
- (void)secureDatabaseByTouchIdWithPath:(NSString *)path
                             completion:(void (^)(id<KLSecureDatabase>))completion
           enterPasswordFallbackHandler:(void (^)())enterPasswordFallbackHandler
                    cancellationHandler:(void (^)(NSError *))cancellationHandler NS_AVAILABLE_IOS(8_0);
/**
 * Returns secure file with the passed path and password. Note that this method doesn't open any file Get password
 * from
 * secure storage by touch id.
 *
 * @param path Secure database file path (UTF-8)
 * @param completion Block return KLSecureFile
 * @param enterPasswordFallbackHandler
 * @param cancellationHandler return cancellation information.
 *
 * Declarate keychain group 'com.kaspersky.sdk' in your project Entitlements.plist"
 */
- (void)secureFileByTouchIdWithPath:(NSString *)path
                         completion:(void (^)(id<KLSecureFile>))completion
       enterPasswordFallbackHandler:(void (^)())enterPasswordFallbackHandler
                cancellationHandler:(void (^)(NSError *))cancellationHandler NS_AVAILABLE_IOS(8_0);

/**
 * Setup TouchId UI localizations. setup to nil for default.
 *
 * @param localizedReason Application reason for authentication. This string must be provided in correct
 *                        localization and should be short and clear. It will be eventually displayed in
 *                        the authentication dialog subtitle. A name of the calling application will be
 *                        already displayed in title, so it should not be duplicated here.
 * @param localizedFallbackTitle Fallback button title. Allows fallback button title customization.
 *                               A default title "Enter Password" is used when this property is left nil. If set to
 *                               empty string, the button will be hidden.
 */
- (void)setupTouchIdWithlocalizedReason:(NSString *)localizedReason
                 localizedFallbackTitle:(NSString *)localizedFallbackTitle NS_AVAILABLE_IOS(8_0);

/**
 * Bind TouchId with path and password. Universal method for all files.
 *
 * @param path Secure file path (UTF-8)
 * @param password Secure file password
 *
 * Declarate keychain group 'com.kaspersky.sdk' in your project Entitlements.plist"
 */
- (bool)bindTouchIdWithPath:(NSString *)path password:(NSString *)password NS_AVAILABLE_IOS(8_0);

@end
