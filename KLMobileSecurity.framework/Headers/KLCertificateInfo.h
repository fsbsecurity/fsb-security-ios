#import "KLX509Certificate.h"


/**
 * The KLCertificateVerdict enumerator provides the verdicts of SSL certificate checking.
 */
typedef enum {
    kKLCertificateVerdict_Trusted,   // Trusted certificate
    kKLCertificateVerdict_Untrusted, // Untrusted certificate
    kKLCertificateVerdict_Unknown    // Unknown certidicate
} KLCertificateVerdict;

/**
 * The KLCertificateVerdict enumerator provides the  extended verdicts of SSL certificate checking.
 */
typedef enum {
    kKLCertificateExtendedVerdict_Unspecified,        // Unspecified verdict
    kKLCertificateExtendedVerdict_CertificateRevoked, // Certificate revoked
    kKLCertificateExtendedVerdict_FakeCertificate,    // Fake certificate
    kKLCertificateExtendedVerdict_InvalidChain,       // Invalid certificate chain
    kKLCertificateExtendedVerdict_DomainNotMatch,     // Certificate and site domains don't match
    kKLCertificateExtendedVerdict_InvalidPurpose,     // Invalid certificate purpose
    kKLCertificateExtendedVerdict_InvalidTime,        // Invalid certificate time
    kKLCertificateExtendedVerdict_InvalidStructure    // Invalid certificate structure
} KLCertificateExtendedVerdict;


/**
 * The protocol provides the verdict of SSL certificate verification.
 */
@protocol KLCertificateInfo <NSObject>

/**
 * Returns a certificate verification verdict.
 */
@property (nonatomic, readonly) KLCertificateVerdict verdict;

/**
 * Returns an extended certificate verification verdict.
 */
@property (nonatomic, readonly) KLCertificateExtendedVerdict extendedVerdict;

/**
 * Returns a certificate information.
 */
@property (nonatomic, readonly) id<KLX509Certificate> certificate;

@end
