#import "KLSecureConnection.h"


/**
 * The protocol provides methods for creating secure connection.
 */
@protocol KLSecureConnectionManager <NSObject>

/**
 * Creates an KLSecureConnection instance.
 *
 * @param request The URL request to load.
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An instance of KLSecureConnection protocol.
 */
- (id<KLSecureConnection>)createSecureConnectionWithRequest:(NSURLRequest*)request error:(NSError* __autoreleasing *)errorPtr;

@end
