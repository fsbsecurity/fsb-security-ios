#import <Foundation/Foundation.h>

/**
 * Row iteration block
 *
 * @param rowValues Row values
 *
 * @return YES to continue iteration, NO - to interrupt
 */
typedef BOOL (^KLSecureDatabaseRowIterationBlock)(NSDictionary *rowValues);

/**
 * KLSecureDatabase statement
 */
@protocol KLSecureDatabaseStatement <NSObject>

/**
 * SQL query the statement is based on
 */
@property (nonatomic, readonly) NSString *query;

/**
 * Statement column count
 */
@property (nonatomic, readonly) NSUInteger columnCount;

/**
 * Bind statement integer parameter
 *
 * @param param Parameter value
 * @param key Parameter key (including "@" prefix, e.g. "@param1")
 */
- (void)bindIntegerParam:(NSInteger)param forKey:(NSString *)key;

/**
 * Binds statement double parameter
 *
 * @param param Parameter value
 * @param key Parameter key (including "@" prefix, e.g. "@param1")
 */
- (void)bindDoubleParam:(double)param forKey:(NSString *)key;

/**
 * Binds statement string parameter
 *
 * @param param Parameter value
 * @param key Parameter key (including "@" prefix, e.g. "@param1")
 */
- (void)bindStringParam:(NSString *)param forKey:(NSString *)key;

/**
 * Binds statement BLOB parameter
 *
 * @param param Parameter value
 * @param key Parameter key (including "@" prefix, e.g. "@param1")
 */
- (void)bindBlobParam:(NSData *)param forKey:(NSString *)key;

/**
 * Clears statement bindings
 */
- (void)clearBindings;

/**
 * Resolves column name by its index
 *
 * @param index Column index
 * @return Column name
 */
- (NSString *)columnNameAtIndex:(NSUInteger)index;

/**
 * Resolves column index by its index
 *
 * @param name Column name
 * @return Column index or NSNotFound value if column not found
 */
- (NSInteger)columnIndexByName:(NSString *)name;

/**
 * Executes statement and iterates over the resulting data executing rowIterationBlock on
 * every row. Iteration stops if block returns NO;
 *
 * @param rowIterationBlock Block executed on every resulting data row
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return YES in case of success otherwise NO
 */
- (BOOL)execAndIterateOverResultsWithBlock:(KLSecureDatabaseRowIterationBlock)rowIterationBlock
                                     error:(NSError *__autoreleasing *)error;

/**
 * Executes statement and collects the result data as the array of rows. Each row is represented as
 * the array of values. Use statement columnNameAtIndex and columnIndexByName methods to retrieve columns
 * information
 *
 * @param resultRows Result rows array. If NULL value passed method works like execAndIgnoreResult.
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return YES in case of success otherwise NO
 */
- (BOOL)execAndCollectResults:(NSArray *__autoreleasing *)resultRows error:(NSError *__autoreleasing *)error;

/**
 * Executes statement and ignores the result data
 *
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return YES in case of success otherwise NO
 */
- (BOOL)execAndIgnoreResultsWithError:(NSError *__autoreleasing *)error;

@end
