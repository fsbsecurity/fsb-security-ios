#import "KLIPAddress.h"

/**
 * The KLDnsVerificationVerdict enumerator provides verdict of dns verification operation
 */
typedef enum {
    kKLDnsVerificationVerdict_Unknown,
    kKLDnsVerificationVerdict_Untrusted,
    kKLDnsVerificationVerdict_Trusted,
    kKLDnsVerificationVerdict_Partial
} KLDnsVerificationVerdict;


/**
 * The protocol provides information about result of check dns operation.
 */
@protocol KLDnsVerificationResult <NSObject>

/**
 * Verification verdict
 */
@property (nonatomic, readonly) KLDnsVerificationVerdict verdict;

/**
 * Trusted Ip addreses
 */
@property (nonatomic, readonly) NSArray *trustedIpAddresses;

@end
