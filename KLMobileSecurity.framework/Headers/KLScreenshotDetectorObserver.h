#import <Foundation/Foundation.h>

@protocol KLScreenshotDetector;


/**
 * Screenshot detector observer protocol
 */
@protocol KLScreenshotDetectorObserver<NSObject>

/**
 * Called after screenshot captured by user
 */
- (void)screenshotDetectorDidTakeScreenshot:(id<KLScreenshotDetector>)screenshotDetector;

@end