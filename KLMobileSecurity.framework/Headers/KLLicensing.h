#import "KLPerUserLicensing.h"
#import "KLLicenseInfo.h"


/**
 * The KLLicensing protocol provides methods for managing license
 */
@protocol KLLicensing <KLPerUserLicensing>

/**
 * An information about current license
 */
@property (nonatomic, readonly) id<KLLicenseInfo> licenseInfo;

/**
 * Activates application using code from SDK config
 *
 * @param successHandler The block called when app successfully activated
 * @param errorHandler The block called if an error occurs.
 */
- (void)activateWithSuccessHandler:(KLLicensingSuccessHandler)successHandler errorHandler:(KLLicensingErrorHandler)errorHandler;

@end