#import "KLUrlCheckerErrors.h"
#import "KLUrlCheckResult.h"


/**
 * The KLUrlCheckerSource enumerator provides the sources for Url checker.
 */
typedef enum {
    kKLUrlCheckerSource_CloudService,       // Using Kaspersky Security Network cloud service
    kKLUrlCheckerSource_PaymentCategorizer, // Using offline payment sites categorizer
    kKLUrlCheckerSource_All                 // All sources
} KLUrlCheckerSource;


/**
 * The protocol provides methods for categorizing URLs.
 */
@protocol KLUrlChecker <NSObject>

/**
 * Checks URL using KSN.
 *
 * @param url A URL to retrieve the information about.
 * @param source A url categorization source.
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An object which provides the information whether the URL provides any threats.
 */
- (id<KLUrlCheckResult>)checkUrl:(NSURL *)url
                          source:(KLUrlCheckerSource)source
                           error:(NSError * __autoreleasing *)errorPtr;

/**
 * The method normalizes url string and checks it
 *
 * @param urlString A string of URL to retrieve the information about.
 * @param source A url categorization source.
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An object which provides the information whether the URL provides any threats.
 */
- (id<KLUrlCheckResult>)checkUrlString:(NSString *)urlString
                                source:(KLUrlCheckerSource)source
                                 error:(NSError* __autoreleasing *)errorPtr;

@end
