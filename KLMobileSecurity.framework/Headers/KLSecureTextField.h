#import "KLSecureControl.h"
#import <UIKit/NSText.h>
#import <UIKit/UITextInput.h>

typedef NS_ENUM(NSInteger, KLSecureTextFieldViewMode) {
    kKLSecureTextFieldViewMode_Never,
    kKLSecureTextFieldViewMode_WhileEditing,
    kKLSecureTextFieldViewMode_UnlessEditing,
    kKLSecureTextFieldViewMode_Always
};

@protocol KLSecureTextFieldDelegate;

/**
 * @abstract Secure text field.
 *
 * @discussion KLSecureTextField is a safe replacement for UITextField class. Text input in KLSecureTextField is
 *             protected from the all known keyloggers on iOS.
 */
__attribute__((visibility("default")))
@interface KLSecureTextField : KLSecureControl <UITextInput>

/**
 * @abstract The text represented by the text field. The text is displayed until secureTextEntry is YES.
 *
 * @discussion The default text is @"".
 */
@property (nonatomic, copy) NSString *text;

/**
 * @abstract The color of the text.
 *
 * @discussion The default text color is black.
 */
@property (nonatomic, strong) UIColor *textColor;

/**
 * @abstract The font of the text.
 *
 * @discussion This property applies to the entire text of the text field and to the placeholder text.
 *             The default font is 12pt Helvetica.
 */
@property (nonatomic, strong) UIFont *font;

/**
 * @abstract The technique to use for aligning the text.
 */
@property (nonatomic, assign) NSTextAlignment textAlignment;

/**
 * @abstract The string  that is displayed when there is no other text in the text field.
 *
 * @discussion The default placeholder is @"".
 */
@property (nonatomic, copy) NSString *placeholder;

/**
 * @abstract The color of the placeholder.
 *
 * @discussion The default placeholder color is 75% gray.
 */
@property (nonatomic, strong) UIColor *placeholderColor;

/**
 * @abstract A boolean value indicating whether the text field removes old text when editing begins.
 */
@property (nonatomic, assign) BOOL clearsOnBeginEditing;

/**
 * @abstract The receiver's delegate.
 *
 * @discussion A text field delegate responds to editing-related messages from the text field. You can use the delegate
 *             to respond to the text entered by the user and to some special commands, such as when the return button
 *             is pressed.
 */
@property (nonatomic, weak) IBOutlet id<KLSecureTextFieldDelegate> delegate;

/**
 * @abstract A boolean value indicating whether the text field is currently in edit mode.
 */
@property (nonatomic, readonly, getter=isEditing) BOOL editing;

/**
 * @abstract Controls when the standard clear button appears in the text field.
 */
@property (nonatomic, assign) KLSecureTextFieldViewMode clearButtonMode;

/**
 * @abstract The overlay view displayed on the left side of the text field.
 */
@property (nonatomic, strong) IBOutlet UIView *leftView;

/**
 * @abstract Controls when the left overlay view appears in the text field.
 */
@property (nonatomic, assign) KLSecureTextFieldViewMode leftViewMode;

/**
 * @abstract The overlay view displayed on the right side of the text field.
 */
@property (nonatomic, strong) IBOutlet UIView *rightView;

/**
 * @abstract Controls when the right overlay view appears in the text field.
 */
@property (nonatomic, assign) KLSecureTextFieldViewMode rightViewMode;

/**
 * @abstract Padding which defines the bounding box for content (text input).
 */
@property (nonatomic, assign) UIEdgeInsets contentPadding;

/**
 * @abstract If NO then during text input the last character is not obscured for a while. Otherwise all characters are
 * always obscured.
 *
 * @discussion This property is taken into account when secureTextEntry is YES.
 */
@property (nonatomic, assign, getter=shouldAlwaysObscureLastCharacter) BOOL alwaysObscureLastCharacter;

/**
 * @abstract The custom input view to display when the text field becomes the first responder.
 */
@property (nonatomic, strong) UIView *inputView;

@end

/**
 * @abstract The KLSecureTextFieldDelegate protocol defines the messages sent to a text field delegate as part of the
 * sequence of editing its text.
 */
@protocol KLSecureTextFieldDelegate <NSObject>

@optional

/**
 * @abstract Asks the delegate if editing should begin in the specified text field.
 *
 * @return YES if an editing session should be initiated. Otherwise, NO to disallow editing.
 */
- (BOOL)secureTextFieldShouldBeginEditing:(KLSecureTextField *)textField;

/**
 * @abstract Tells the delegate that editing began for the specified text field.
 */
- (void)secureTextFieldDidBeginEditing:(KLSecureTextField *)textField;

/**
 * @abstract Asks the delegate if editing should stop in the specified text field.
 *
 * @return YES if editing should stop. Otherwise, NO if the editing session should continue.
 */
- (BOOL)secureTextFieldShouldEndEditing:(KLSecureTextField *)textField;

/**
 * @abstract Tells the delegate that editing stopped for the specified text field.
 */
- (void)secureTextFieldDidEndEditing:(KLSecureTextField *)textField;

/**
 * @abstract Asks the delegate if the specified text should be changed.
 *
 * @return YES if the specified text range should be replaced. Otherwise, NO to keep the old text.
 */
- (BOOL)secureTextField:(KLSecureTextField *)textField
    shouldChangeCharactersInRange:(NSRange)range
                replacementString:(NSString *)string;

/**
 * @abstract Asks the delegate if the text field's current contents should be removed.
 *
 * @return YES if the text field's contents should be cleared. Otherwise, NO.
 */
- (BOOL)secureTextFieldShouldClear:(KLSecureTextField *)textField;

/**
 * @abstract Asks the delegate if the text field should process the pressing of the return button.
 *
 * @return YES if the text field should implement its default behavior for the return button. Otherwise, NO.
 */
- (BOOL)secureTextFieldShouldReturn:(KLSecureTextField *)textField;

@end
