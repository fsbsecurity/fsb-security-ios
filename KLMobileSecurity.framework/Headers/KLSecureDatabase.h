#import "KLSecureDatabaseErrors.h"
#import "KLSecureDatabaseStatement.h"
#import <Foundation/Foundation.h>

/**
 * Secure database (encrypted database)
 *
 * Use [KLSecureDatabaseManager secureDatabaseWithPath:] to get secure Database instances
 */
@protocol KLSecureDatabase <NSObject>

/**
 * Opens secure database. If there is no database at the path new database will be created
 *
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return YES in case of success otherwise NO
 */
- (BOOL)openWithError:(NSError *__autoreleasing *)error;

/**
 * Closes secure database. Closing not opened or closed secure database doesn't do anything
 *
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return YES in case of success otherwise NO
 */
- (BOOL)closeWithError:(NSError *__autoreleasing *)error;

/**
 * Check if the secure database open
 *
 * @return YES if the database open otherwise NO
 */
- (BOOL)isOpen;

/**
 * Create statement with SQL query
 *
 * @param query SQL query. Query examples: "CREATE TABLE A(ID INTEGER, NAME TEXT)", "INSERT INTO A(ID, NAME) VALUES(1,
 * "TEST")" etc. Query may contain parameter templates (e.g. "@param1"), this templates will be replaced with actual
 * values as the result of the parameters binding
 *
 * @param error If an error occurs, upon return contains an NSError object that describes the problem.
 *
 * @return prepared statement
 */
- (id<KLSecureDatabaseStatement>)createStatementWithQuery:(NSString *)query error:(NSError *__autoreleasing *)error;

/**
 * Run transaction
 *
 * @param transactionBlock Transaction block to be executed
 */
- (void)runTransaction:(void (^)())transactionBlock;

#pragma mark - TouchID

/**
 * Bind touchID and password.
 * Use [KLSecureDatabaseManager secureDatabaseByTouchIdWithPath: completion:] to get secure Database instances by
 * touchID
 *
 * Declarate keychain group 'com.kaspersky.sdk' in your project Entitlements.plist
 *
 */
- (BOOL)bindTouchId;

/**
 * Unbind touchID and password.
 */
- (void)unBindTouchId;

@end
