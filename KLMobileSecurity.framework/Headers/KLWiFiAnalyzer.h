#import "KLWifiAnalyzerErrors.h"
#import "KLWifiInfo.h"


/**
 * The protocol provides methods for Wi-Fi analyzation
 */
@protocol KLWiFiAnalyzer <NSObject>

/**
 * Receives information about current connected Wi-Fi access point
 *
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An object provides information abount current Wi-Fi
 */
- (id<KLWiFiInfo>)currentWiFiInfoWithError:(NSError* __autoreleasing*)errorPtr;

@end
