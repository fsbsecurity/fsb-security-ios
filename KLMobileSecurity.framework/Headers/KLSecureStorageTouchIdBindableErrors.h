#import <Foundation/Foundation.h>

/**
 * Secure storage Touch ID error codes
 */
typedef enum {
    KLSecureStorageTouchIdBindableErrorCode_Unknown,
    KLSecureStorageTouchIdBindableErrorCode_FileNotFound,
    KLSecureStorageTouchIdBindableErrorCode_DocumentIdentifierKeyNotFound,
} KLSecureStorageTouchIdBindableErrorCode;

/**
 * Secure storage Touch ID domain
 */
extern NSString *const kKLSecureStorageTouchIdBindableDomain;