#import "KLFraudPreventionConsumer.h"
#import "KLFraudPreventionErrors.h"
#import "KLFraudPreventionTypes.h"
#import <CoreLocation/CoreLocation.h>

/**
 * The protocol provides methods to integration with Kaspersky AntiFraud system
 */
@protocol KLFraudPreventionManager <NSObject>

/**
 * The method add consumer identifier and url with certificate.
 *
 * @param consumerId The consumerId Id
 */

- (void)addConsumer:(KLFraudPreventionConsumer *)consumer;

/**
 * The method send user identifier to antifraud system.
 *
 * @param userIdentifier The unique user Id
 * @param successHandler The block called when user id successfully sent.
 * @param errorHandler The block called if an error occurs.
 */
- (void)sendUserIdentifier:(NSString *)userIdentifier
            successHandler:(KLFraudPreventionSuccessHandler)successHandler
              errorHandler:(KLFraudPreventionErrorHandler)errorHandler;

/**
 * The method send simulator or device to antifraud system.
 *
 * @param successHandler The block called when info sent.
 * @param errorHandler The block called if an error occurs.
 */
- (void)sendDeviceSimulatorWithSuccessHandler:(KLFraudPreventionSuccessHandler)successHandler
                                 errorHandler:(KLFraudPreventionErrorHandler)errorHandler;

/**
 * The method send custom action to antifraud system.
 *
 * @param actionType
 * @param actionResult
 * @param actionDescription     
 */
- (void)sendMobApplicationActionType:(NSString *)actionType
                        actionResult:(NSString *)actionResult
                   actionDescription:(NSString *)actionDescription;

#pragma mark - sendDeviceCoordinates

/**
 * The method send device coordinates to antifraud system.
 *
 * @param successHandler The block called when coordinates sent.
 * @param errorHandler The block called if an error occurs.
 */
- (void)sendDeviceCoordinatesWithSuccessHandler:(KLFraudPreventionSuccessHandler)successHandler
                                   errorHandler:(KLFraudPreventionErrorHandler)errorHandler;

/**
 * The method send device coordinates to antifraud system.
 *
 * @param desiredLocationAccuracy Desired location detection accuracy (value of CLLocation.horizontalAccuracy).
 * @param successHandler The block called when user id successfully sent
 * @param errorHandler The block called if an error occurs.
 */
- (void)sendDeviceCoordinatesWithAccuracy:(CLLocationAccuracy)desiredLocationAccuracy
                           SuccessHandler:(KLFraudPreventionSuccessHandler)successHandler
                             errorHandler:(KLFraudPreventionErrorHandler)errorHandler;

/**
 * The method send device coordinates to antifraud system.
 *
 * @param desiredLocationAccuracy Desired location detection accuracy (value of CLLocation.horizontalAccuracy).
 * @param maxDetectionDuration Maximum duration of location detection.
 * @param successHandler The block called when user id successfully sent
 * @param errorHandler The block called if an error occurs.
 */
- (void)sendDeviceCoordinatesWithAccuracy:(CLLocationAccuracy)desiredLocationAccuracy
                     maxDetectionDuration:(NSTimeInterval)maxLocationDetectionDuration
                           SuccessHandler:(KLFraudPreventionSuccessHandler)successHandler
                             errorHandler:(KLFraudPreventionErrorHandler)errorHandler;

@end