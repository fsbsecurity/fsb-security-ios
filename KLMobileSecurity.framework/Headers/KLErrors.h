#import <Foundation/Foundation.h>


/**
 * The KLErrorCode enumerator provides the error codes which may occur in SDK.
 */
typedef enum {
    kKLErrorCode_Unknown,
    kKLErrorCode_InvalidArgument,
    kKLErrorCode_IncorrectConfiguration
} KLErrorCode;

/**
 * Kaspersky SDK error domain
 */
extern NSString* const kKLErrorDomain;
