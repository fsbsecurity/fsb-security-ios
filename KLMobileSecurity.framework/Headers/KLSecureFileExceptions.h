#import <Foundation/Foundation.h>

/**
 * Secure file operation exception
 */
extern NSString *kKLSecureFileOperationException;
