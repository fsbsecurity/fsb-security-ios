#import <Foundation/Foundation.h>


/**
 * @enum KLSelfDefenceAppSignatureErrorCode
 * @abstract KLSelfDefenceAppSignature error codes
 */
typedef enum {
    kKLSecureKeyboardErrorCode_Unknown,
    kKLSecureKeyboardErrorCode_InvalidLayoutFormat,
} KLSecureKeyboardErrorCode;

/**
 * @const kKLSecureKeyboardErrorDomain
 * @abstract KLSecureKeyboard error domain
 */
extern NSString* const kKLSecureKeyboardErrorDomain;
