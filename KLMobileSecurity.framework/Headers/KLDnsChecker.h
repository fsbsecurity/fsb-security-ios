#import "KLDnsCheckerErrors.h"
#import "KLDnsVerificationResult.h"


/**
 * The KLDnsChecker protocol provides methods to verifying DNS for web urls.
 */
@protocol KLDnsChecker <NSObject>

/**
 * Verifies that DNS for the URL on routing will not be modified.
 *
 * @param url A web url.
 * @param errorPtr If an error occurs, upon return contains an NSError object that describes the problem.
 * @return An object which provides the information about dns verification.
 */
- (id<KLDnsVerificationResult>)verifyDnsForUrl:(NSURL*)url error:(NSError* __autoreleasing *)errorPtr;

@end
