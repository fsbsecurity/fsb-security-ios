#import "KLSecureDatabase.h"
#import "KLSecureFile.h"
#import "KLSecureStorageTouchIdBindableManager.h"

@protocol KLSecureStorageManager <KLSecureStorageTouchIdBindableManager, NSObject>

/**
 * Returns secure database with the passed path. Note that this method doesn't open or create
 * any file
 *
 * @param path Secure database file path (UTF-8)
 * @param password Secure database password
 *
 * @return secure database
 */
- (id<KLSecureDatabase>)secureDatabaseWithPath:(NSString *)path password:(NSString *)password;

/**
 * Returns secure file with the passed path and password. Note that this method doesn't open or create
 * any file
 *
 * @param path Secure file path (UTF-8)
 * @param password Secure file password
 *
 * @return secure file
 */
- (id<KLSecureFile>)secureFileWithPath:(NSString *)path password:(NSString *)password;

@end
