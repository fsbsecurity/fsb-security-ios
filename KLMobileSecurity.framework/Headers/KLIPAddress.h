#import <Foundation/Foundation.h>

typedef enum {
    kKLIPAddressType_Unknown,
    kKLIPAddressType_IPV4,
    kKLIPAddressType_IPV6
} KLIPAddressType;


/**
 * The KLIPAddress class provides an Internet Protocol (IP) address.
 */
__attribute__((visibility("default")))
@interface KLIPAddress : NSObject

/**
 * Creates a new instance of the KLIPAddress class with the address specified as a byte array.
 *
 * @param bytes IP address specified as 4 bytes array.
 * @return A KLIPAddress instance. Returns nil if the KLIPAddress object could not be created.
 */
+ (instancetype)ipAddressWithBytes:(NSData*)bytes;

/**
 * Creates a new instance of the KLIPAddress class with the address specified as a string.
 *
 * @param bytes IP address specified as string. For example "192.168.0.1".
 * @return A KLIPAddress instance. Returns nil if the KLIPAddress object could not be created.
 */
+ (instancetype)ipAddressWithString:(NSString*)string;

/**
 * Initializes a KLIPAddress object with the address specified as a Byte array.
 *
 * @param bytes IP address specified as 4 bytes array.
 * @return A KLIPAddress instance. Returns nil if the KLIPAddress object could not be created.
 */
- (instancetype)initWithBytes:(NSData*)bytes;

/**
 * Initializes a KLIPAddress object with the address specified as a string.
 *
 * @param bytes IP address specified as string. For example "192.168.0.1".
 * @return A KLIPAddress instance. Returns nil if the KLIPAddress object could not be created.
 */
- (instancetype)initWithString:(NSString*)string;

/**
 * Returns type of IP address
 *
 * @return one of KLIPAddressType constant
 */
- (KLIPAddressType)type;

/**
 * Converts an IP address to byte array.
 *
 * @return A string that contains the IP address.
 */
- (NSData*)toBytes;

/**
 * Converts an IP address to string.
 *
 * @return A string that contains the IP address.
 */
- (NSString*)toString;

/**
* Returns a Boolean value that indicates whether a given address is equal to the receiver
*/
- (BOOL)isEqualToIpAddress:(KLIPAddress*)ipAddress;

@end
