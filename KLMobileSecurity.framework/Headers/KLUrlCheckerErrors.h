#import <Foundation/Foundation.h>


/**
 * Url checker error codes
 */
typedef enum {
    kKLUrlCheckerErrorCode_Unknown,
    kKLUrlCheckerErrorCode_InvalidArgument,
    kKLUrlCheckerErrorCode_ConnectionError,
    kKLUrlCheckerErrorCode_InformationAboutURLNotExists
} KLUrlCheckerErrorCode;

/**
 * Url checker error domain
 */
extern NSString* const kKLUrlCheckerErrorDomain;