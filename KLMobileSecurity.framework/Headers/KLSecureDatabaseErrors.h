#import <Foundation/Foundation.h>

/**
 * Secure database error codes
 */
typedef enum {
    kKLSecureDatabaseErrorCode_Unknown,
    kKLSecureDatabaseErrorCode_InvalidPassword,
    kKLSecureDatabaseErrorCode_DatabaseError,
    kKLSecureDatabaseErrorCode_InternalError,
    kKLSecureDatabaseErrorCode_Misuse
} KLSecureDatabaseErrorCode;

/**
 * Secure database error domain
 */
extern NSString *const kKLSecureDatabaseErrorDomain;
