#import <Foundation/Foundation.h>


/**
 * Firmware checker error codes
 */
typedef enum {
    kKLFirmwareCheckerErrorCode_Unknown
} KLFirmwareCheckerErrorCode;

/**
 * Firmware error domain
 */
extern NSString* const kKLFirmwareCheckerErrorDomain;