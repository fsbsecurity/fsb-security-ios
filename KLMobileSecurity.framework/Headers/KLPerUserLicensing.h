#import "KLLicensingErrors.h"
#import "KLLicensingTypes.h"


/**
 * The KLPerUserLicensing protocol provides method to managing license with per-user licensing model.
 */
@protocol KLPerUserLicensing <NSObject>

/**
 * The method send user identifier to activation server.
 * The user id must be sent for per-user licensing model.
 *
 * @param userId The unique user Id
 * @param successHandler The block called when user id successfully sent
 * @param errorHandler The block called if an error occurs.
 */
- (void)sendUserId:(NSString*)userId successHandler:(KLLicensingSuccessHandler)successHandler errorHandler:(KLLicensingErrorHandler)errorHandler;

@end
