#import <Foundation/Foundation.h>


/**
 * DNS checker error codes
 */
typedef enum {
    kKLDnsCheckerErrorCode_Unknown,
    kKLDnsCheckerErrorCode_InvalidArgument
} KLDnsCheckerErrorCode;

/**
 * DNS checker error domain
 */
extern NSString* const kKLDnsCheckerErrorDomain;