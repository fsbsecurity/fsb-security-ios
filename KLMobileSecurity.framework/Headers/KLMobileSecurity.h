#import "KLErrors.h"
#import "KLConfiguration.h"
#import "KLSDKState.h"
#import "KLUrlChecker.h"
#import "KLCertificateChecker.h"
#import "KLRootDetector.h"
#import "KLScreenshotDetector.h"
#import "KLDnsChecker.h"
#import "KLFingerprint.h"
#import "KLFirmwareChecker.h"
#import "KLSecureStorageManager.h"
#import "KLSecureConnectionManager.h"
#import "KLSecureTextField.h"
#import "KLSecureKeyboard.h"
#import "KLSelfDefence.h"
#import "KLWiFiAnalyzer.h"
#import "KLFraudPreventionManager.h"
#import "KLUpdater.h"
#import "KLLicensing.h"


/**
 * The root class of Kaspersky SDK. The KLMobileSecurity class provides an interface for getting the SDK components.
 */
__attribute__((visibility("default")))
@interface KLMobileSecurity : NSObject

/**
 * Creates an instance of KLMobileSecurity class with specified configuration
 *
 * @param configuration An object that provides configuration parameters: working directory, flags and etc.
 * @param errorPtr If an error occurs, uppon return contains an NSError object that describes the problem.
 * @return An KLMobileSecurity instance. Returns nil if the object couldn't be created.
 */
+ (instancetype)createWithConfiguration:(KLConfiguration*)configuration error:(NSError* __autoreleasing *)errorPtr;

/**
 * Init an instance of KLMobileSecurity class with specified configuration
 *
 * @param configuration An object that provides configuration parameters: working directory, flags and etc.
 * @param errorPtr If an error occurs, uppon return contains an NSError object that describes the problem.
 * @return An KLMobileSecurity instance. Returns nil if the object couldn't be initialized.
 */
- (instancetype)initWithConfiguration:(KLConfiguration*)configuration error:(NSError* __autoreleasing *)errorPtr;

/**
 * Returns current SDK state.
 */
@property (readonly) KLSDKState state;

/**
 * Returns an KLUrlChecker instance which is used for categorizing of URLs.
 */
@property (readonly) id<KLUrlChecker> urlChecker;

/**
 * Returns an KLUrlChecker instance which is used to verify SSL certificates.
 */
@property (readonly) id<KLCertificateChecker> certificateChecker;

/**
 * Returns an KLRootDetector instance which is used to detect whether the device has been rooted.
 */
@property (readonly) id<KLRootDetector> rootDetector;

/**
 * Returns an KLScreenshotDetector instance which is used to detect screenshot capture.
 */
@property (readonly) id<KLScreenshotDetector> screenshotDetector;

/**
 * Returns an KLDnsChecker instance which is used to verify DNS for web urls.
 */
@property (readonly) id<KLDnsChecker> dnsChecker;

/**
 * Returns an KLFingerprint instance which is used for device identifying.
 */
@property (readonly) id<KLFingerprint> fingerprint;

/**
 * Returns an KLFirmwareChecker instance which is used for firmware veridication.
 */
@property (readonly) id<KLFirmwareChecker> firmwareChecker;

/**
 * Returns an KLSecureStorage (encrypted database) manager instance.
 */
@property (readonly) id<KLSecureStorageManager> secureStorageManager;

/**
 * Returns an KLSecureConnectionManager which is used to create protected connection.
 */
@property (readonly) id<KLSecureConnectionManager> secureConnectionManager;

/**
 * Returns an KLSelfDefence instance which is used to protect the application from malicious modifications and debugging.
 */
@property (readonly) id<KLSelfDefence> selfDefence;

/**
 * Returns an KLWiFiAnalyzer instance which is used for analyzation Wi-Fi access points.
 */
@property (readonly) id<KLWiFiAnalyzer> wifiAnalyzer;

/**
* Returns an KLFraudPreventionManager instance which is used for intragraion with AntiFraud system
*/
@property (readonly) id<KLFraudPreventionManager> fraudPrevention;

/**
 * Returns an KLUpdater instance which is used for updating SDK's data files.
 */
@property (readonly) id<KLUpdater> updater;

/**
 * Returns an KLLicensing instance which is used for application licensing management.
 */
@property (readonly) id<KLLicensing> licensing;

@end
