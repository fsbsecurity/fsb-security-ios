#import <Foundation/Foundation.h>


/**
 * WiFi analyzer error codes
 */
typedef enum {
    kKLWiFiAnalyzerErrorCode_Unknown,
    kKLWiFiAnalyzerErrorCode_WiFiNotUsed
} KLWiFiAnalyzerErrorCode;

/**
 * WiFi analyzer error domain
 */
extern NSString* const kKLWiFiAnalyzerErrorDomain;