#import <Foundation/Foundation.h>


/**
 * The KLSelfDefence protocol provides methods for protecting the application from debugging.
 */
@protocol KLSelfDefenceDebugging <NSObject>

/**
 * Checks the presense of debugger.
 *
 * @return Returns TRUE if debugger is connected to the application.
 */
- (BOOL)isDebuggerPresent;

@end
