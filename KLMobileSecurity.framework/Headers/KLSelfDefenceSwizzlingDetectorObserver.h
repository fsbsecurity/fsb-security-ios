#import "KLSelfDefenceSwizzlingInfo.h"

@protocol KLSelfDefenceSwizzlingDetector;


/**
 * Self-defence swizzling detector observer protocol
 */
@protocol KLSelfDefenceSwizzlingDetectorObserver <NSObject>
@optional

/**
 * Called when swizzling object detected.
 *
 * @param swizzlingDetector An KLSelfDefenceSwizzlingDetector instance.
 * @param swizzlingInfo An KLSelfDefenceSwizzlingInfo instance provides information about swizzled object.
 */
- (void)swizzlingDetector:(id<KLSelfDefenceSwizzlingDetector>)swizzlingDetector didDetectSwizzling:(id<KLSelfDefenceSwizzlingInfo>)swizzlingInfo;

@end