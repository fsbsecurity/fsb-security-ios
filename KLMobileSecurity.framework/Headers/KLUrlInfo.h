#import <Foundation/Foundation.h>

/**
 * The KLUrlCategory enumerator provides the information about the category of the webpage content.
 */
typedef enum {
    // Kaspersky Security Network categories
    kKLUrlCategory_Porno,                          // Porno, erotic materials
    kKLUrlCategory_IllegalSoftware,                // Illegal software
    kKLUrlCategory_Drugs,                          // Drugs
    kKLUrlCategory_Violence,                       // Violence
    kKLUrlCategory_Uncensored,                     // Uncensored
    kKLUrlCategory_Weapons,                        // Weapons
    kKLUrlCategory_Gambling,                       // Gambling
    kKLUrlCategory_ForumChat,                      // Forum/chat
    kKLUrlCategory_WebMail,                        // Web-mail
    kKLUrlCategory_OnlineShop,                     // Online shop
    kKLUrlCategory_SocialNetwork,                  // Social network
    kKLUrlCategory_JobSite,                        // Job site
    kKLUrlCategory_AnonymousProxy,                 // Anonymous proxy
    kKLUrlCategory_PaymentByCreditCard,            // Payment by credit card
    kKLUrlCategory_CasualGame,                     // Casual game
    kKLUrlCategory_PoliceDecision,                 // Police Decision (currently only from Japan)
    kKLUrlCategory_SoftwareAudioVideo,             // Software, audio, video and all related content
    kKLUrlCategory_GamblingLotteriesSweepstakes,   // Gampling, lotteries, sweepstakes
    kKLUrlCategory_InternetCommunicationMedia,     // All communication media including chats, forums, social networks etc
    kKLUrlCategory_ECommerce,                      // E-commerce
    kKLUrlCategory_ComputerGames,                  // Computer games and related stuff
    kKLUrlCategory_Religions,                      // Religions, religious assotiations
    kKLUrlCategory_News,                           // News media
    kKLUrlCategory_WebSearch,                      // Web search engine
    
    // Payment categories
    kKLUrlCategory_PaymentSystem,                  // Payment system
    kKLUrlCategory_Bank,                           // Bank site
    kKLUrlCategory_WebService,                     // Web service
    kKLUrlCategory_OnlineGame,                     // Online Game
    kKLUrlCategory_OnlineShopWithOwnPaymentSystem  // Online shop with own payment system
} KLUrlCategory;


/**
 * The protocol provides information about the URL retrieved from KSN.
 */
@protocol KLUrlInfo <NSObject>

/**
 * Returns the enumeration of categories the URL belongs to.
 */
@property (nonatomic, readonly, strong) NSSet* categories;

/**
 * Returns TRUE, if the URL contains phishing content.
 */
@property (nonatomic, readonly, assign) BOOL isPhishing;

/**
 * Returns TRUE, if the URL contains malicious content.
 */
@property (nonatomic, readonly, assign) BOOL isMalware;

@end
