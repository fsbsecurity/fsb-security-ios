#import "KLSelfDefenceAppSignatureObserver.h"
#import "KLSelfDefenceAppSignatureErrors.h"


/**
 * The KLSelfDefenceAppSignature protocol provides methods for protecting the application from malicious modifications.
 */
@protocol KLSelfDefenceAppSignature <NSObject>

/**
 * Adds an observer
 *
 * @param observer Observer to be added
 */
- (void)addObserver:(id<KLSelfDefenceAppSignatureObserver>)observer;

/**
 * Removes an observer
 *
 * @param observer Observer to be removed
 */
- (void)removeObserver:(id<KLSelfDefenceAppSignatureObserver>)observer;

@end