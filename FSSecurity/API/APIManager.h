//
//  APIManager.h
//  FSSecurity
//
//  Created by Mayank Chaudhary on 9/27/16.
//  Copyright © 2016 YMediaLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FSPackageType) {
    FSDeviceProtectionPackage,
    FSSecureWifiPackage,
    FSParentalControlPackage,
    FSCloudBackupPackage,
    FSBatteryUsagePackage
};

@interface FSPackageTypeWrapper : NSObject
@property (readwrite) FSPackageType tile;
@end

@interface APIManager : NSObject

+ (instancetype)sharedInstance;

#pragma mark account and subscription management

// pass the FB AccountKit token to the FSAUTH service. return YES on success
- (void) loginUserWithCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure;

// create a user in the FSAUTH system if they have not already been provisioned. return YES on success
- (void) provisionUserWithCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure;

// subscription management
- (BOOL) hasSubscribedToPackage:(FSPackageType)type;
- (void) subscribeToPackage:(FSPackageType)type withCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure;
- (void) unsubscribeFromPackage:(FSPackageType)type withCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure;

#pragma mark device security APIs
- (BOOL) isJailbroken;
- (void) launchSafebrowser;
- (BOOL) isSafebrowserInstalled;


// retrieve all the tiles to be displayed including those that are not currently purchased
@property (readonly) NSArray<FSPackageTypeWrapper *>* homeTiles;

@end
