//
//  APIManager.m
//  FSSecurity
//
//  Created by Mayank Chaudhary on 9/27/16.
//  Copyright © 2016 YMediaLabs. All rights reserved.
//

#import "APIManager.h"
#import <KLMobileSecurity/KLMobileSecurity.h>

// singleton class providing a facade to UI layers.
// also allows for swapping out mock vs real implementations to allow UI and backend development to proceed in parallel
static  KLMobileSecurity* sdk;

@implementation APIManager
+ (instancetype)sharedInstance {
    static APIManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[APIManager alloc] init];
        [_sharedManager initializeSecuritySDK];
    });
    
    return _sharedManager;
}

// Override this if a different implementation is required
- (void) initializeSecuritySDK {
    KLConfiguration *configuration = [KLConfiguration configuration];
#ifdef DEBUG
    configuration.debugging = YES;
#endif
    
    NSError *error = nil;
    KLMobileSecurity *kasperskySDK = [KLMobileSecurity createWithConfiguration:configuration error:&error];
    if (kasperskySDK != nil) {
        if (kasperskySDK.state == kKLSDKState_NotActivated) {
            [kasperskySDK.licensing activateWithSuccessHandler:^{
                sdk = kasperskySDK;
            }
            errorHandler:^(NSError *error) {
                [self showAlertWithTitle:@"Failed to create Kaspersky SDK" message:error.localizedDescription];
            }];
        }
        else {
            sdk = kasperskySDK;
        }
    }
    else {
        [self showAlertWithTitle:@"Failed to create Kaspersky SDK" message:error.localizedDescription];

    }
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    
    [self showAlertWithTitle:title message:message tag:-1];
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message tag:(NSInteger)tag {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    alert.tag = tag;
    [alert show];
}

#pragma mark - capabilities

// pass the FB AccountKit token to the FSAUTH service. return YES on success
- (void) loginUserWithCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure {
    
}

// create a user in the FSAUTH system if they have not already been provisioned. return YES on success
- (void) provisionUserWithCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure {
    
}

// subscription management
- (BOOL) hasSubscribedToPackage:(FSPackageType)type {
    return false;
}

- (void) subscribeToPackage:(FSPackageType)type withCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure {
    
}
- (void) unsubscribeFromPackage:(FSPackageType)type withCompletionBlock:(void(^)())completion andFailureBlock:(void(^)())failure {
    
}

#pragma mark device security APIs
- (BOOL) isJailbroken {
    id<KLRootDetector> rootDetector = sdk.rootDetector;
    return [rootDetector isDeviceRooted];
}
- (void) launchSafebrowser {
    
}
- (BOOL) isSafebrowserInstalled {
    return false;
}




@end
