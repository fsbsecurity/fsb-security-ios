//
//  AppDelegate.h
//  FSSecurity
//
//  Created by Mayank Chaudhary on 9/27/16.
//  Copyright © 2016 YMediaLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

