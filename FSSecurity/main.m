//
//  main.m
//  FSSecurity
//
//  Created by Mayank Chaudhary on 9/27/16.
//  Copyright © 2016 YMediaLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
