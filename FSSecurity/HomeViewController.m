//
//  ViewController.m
//  FSSecurity
//
//  Created by Mayank Chaudhary on 9/27/16.
//  Copyright © 2016 YMediaLabs. All rights reserved.
//

#import "HomeViewController.h"
#import "APIManager.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

// TODO: do we need a blocking call here to refresh license information every time the view appears
// or should we only do this when the app starts up or comes to foreground
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    [[APIManager sharedInstance] loginUserWithCompletionBlock:^{
        // grab home tiles and display them
    } andFailureBlock:^{
        // launch AccountKit onboarding flow
    }];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
